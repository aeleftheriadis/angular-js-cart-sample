// Generated by CoffeeScript 1.7.1
app.controller('indexCheckoutController', function($scope, CheckoutFactory) {
  $scope.types = [
    {
      id: 0,
      title: "Επιλέξτε"
    },
    {
      id: 1,
      title: "Κατάθεση σε Λογαριασμό Τραπέζης"
    },
    {
      id:2,
      title: "Paypal"
    },
      {
          id:3,
          title: "Eurobank"
      }
  ];

  $scope.sendtype = $scope.types[0];

  $scope.removeFromCheckout = function(index) {
    $scope.items.splice(index, 1);
    $scope.items.splice(index - 1, 1);
  };
  $scope.sendOrder = function() {
      console.log($scope.sendtype);
     if($scope.sendtype.id == 2 || $scope.sendtype.id == 3 ){
         return CheckoutFactory.sendOrder($scope.items, $scope.sendtype, $scope.sendcomment).success(function (data) {

             $(".alert-success").html(data.html).fadeIn(400);
         });
     }
     else {
         return CheckoutFactory.sendOrder($scope.items, $scope.sendtype, $scope.sendcomment).success(function (data) {
             $(".alert-success").html(data.successMsg).stop().fadeIn(400).delay(2000).fadeOut(400);
             setTimeout("window.location = '/checkout/success'",2000);
         }).error(function (data) {
             $(".alert-danger").html(data.errors).stop().fadeIn(400).delay(2000).fadeOut(400);
         });
     }

  };
  CheckoutFactory.getCheckout().success(function(data) {
    $scope.items = data;
  });
});




